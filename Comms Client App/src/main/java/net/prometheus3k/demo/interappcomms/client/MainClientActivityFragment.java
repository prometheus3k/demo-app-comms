package net.prometheus3k.demo.interappcomms.client;

import android.os.Bundle;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import net.prometheus3k.libs.droidutils.appcomms.AppCommsClient;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainClientActivityFragment extends Fragment {
    private static final String TAG = MainClientActivityFragment.class.getSimpleName();
    static final String sServerPackage = "net.prometheus3k.demo.interappcomms.server";
    static final String sServerName = "MyAppCommsServer";

    private AppCommsClient mClient;

    public MainClientActivityFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClient = new AppCommsClient(getActivity(), sServerPackage, sServerName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_client, container, false);
        Button pingButton = (Button) view.findViewById(R.id.ping_button);
        pingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle message = new Bundle();
                message.putString("msg", "hello!");

                mClient.sendMessage(message, new Callback() {
                    @Override
                    public boolean handleMessage(Message message) {
                        Log.w(TAG, "handleMessage()");
                        Bundle data = message.getData();

                        if(data != null && data.getString("msg") != null){
                            Log.w(TAG, message.getData().getString("msg"));
                            return true;
                        }
                        return false;
                    }
                });
            }
        });


        return view;
    }
}
