package net.prometheus3k.demo.interappcomms.secureserver;

import net.prometheus3k.demo.interappcomms.secure.common.SecureCommsConstants;
import net.prometheus3k.libs.droidutils.appcomms.SecureAppCommsServer;

/**
 * Created by babumadhikarmi on 20/07/2015.
 */
public class MySecureAppCommsServer extends SecureAppCommsServer {

    public MySecureAppCommsServer() {
        super(SecureCommsConstants.SALT);
    }
}
