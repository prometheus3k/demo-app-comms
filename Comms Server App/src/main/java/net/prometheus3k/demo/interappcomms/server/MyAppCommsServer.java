package net.prometheus3k.demo.interappcomms.server;
//546-1117/? W/ActivityManager﹕ Unable to start service Intent { cmp=net.prometheus3k.demo.interappcomms.server/.MyAppCommsServer (has extras) } U=0: not found
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import net.prometheus3k.libs.droidutils.appcomms.AppCommsServer;

/**
 * Created by babumadhikarmi on 07/07/2015.
 */
public class MyAppCommsServer extends AppCommsServer {
    private static final String TAG = MyAppCommsServer.class.getSimpleName();

    @Override
    public void onMessageReceived(Bundle dataReceived, Messenger outMessenger) {
        Log.w(TAG, "onMessageReceived()");
        Log.w(TAG, dataReceived.getString("msg"));

        //now reply
        if(outMessenger != null){
            Bundle data = new Bundle();
            data.putString("msg", "hello back!");

            Message message = new Message();
            message.setData(data);
            try {
                outMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
