package net.prometheus3k.demo.interappcomms.secureclient;


import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import net.prometheus3k.demo.interappcomms.secure.common.SecureCommsConstants;
import net.prometheus3k.libs.droidutils.appcomms.SecureAppCommsClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainActivityFragment extends Fragment{
    private static final String TAG = MainActivityFragment.class.getSimpleName();
    static final String sServerPackage = "net.prometheus3k.demo.interappcomms.secureserver";
    static final String sServerName = "MySecureAppCommsServer";

    private SecureAppCommsClient mClient;

    public MainActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClient = new SecureAppCommsClient(this.getActivity(), sServerPackage, sServerName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_activity, container, false);

        Button button = (Button)view.findViewById(R.id.secure_ping_button);

        final SecureAppCommsClient.AuthResultHandler authResultHandler = new SecureAppCommsClient.AuthResultHandler() {
            @Override
            public void onResult(SecureAppCommsClient.Result result) {
                Log.w(TAG, "onResult()");
                Toast.makeText(getActivity(), "Auth Challenge: " + result.toString(), Toast.LENGTH_LONG).show();
            }
        };

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClient.sendAuthChallenge(SecureCommsConstants.SALT, authResultHandler);
            }
        });
        return view;
    }
}
