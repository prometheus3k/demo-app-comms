package net.prometheus3k.demo.interappcomms.secure.common;

/**
 * Created by babumadhikarmi on 20/07/2015.
 */
public interface SecureCommsConstants {
    String SALT = "T7gTo" + "Ed3mI" + "nYf" + "gMB8i0" + "aEQtP" + "NfReFQ" +"7KqS" + "LBBxVD";
}
