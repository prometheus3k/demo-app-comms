# README #

### What is this repository for? ###

This project demonstrates communication between apps using appcomms library from the [Droid Utils Library](https://bitbucket.org/prometheus3k/droid-utils)
There are two demos of comms; one is simple comms (send message, get response).
The other is authentication challenge and response.

Inter App Comms assumes a Client/Server model. The Server component of your app, would declare your Server subclass as a Service (it is actually an IntentService).

**Use cases:**

* Simple: Send a message to a specific app and get a response. 

* Secure: You may want one app to detect the presence of another installed app. As it's easy to spoof a package name, a package check would not suffice. Secure Client can send a hash, recieves and compares a hash returned by the Server.

**Requirements:**

* Android Studio

* Android API level 14+

### How do I get set up? ###
Checking out the project should be enough to run the demos. To integrate with your projects (assuming you have two android apps, designate one as server app and the other as client app). As mentioned there are two implementations of App Comm Server/Client. Common to both is importing my [Droid Utils Library](https://bitbucket.org/prometheus3k/droid-utils) into Client AND Server apps.

**Simple AppComms:**

1. Create a subclass of AppCommsServer, overriding onMessageReceived()

2. In the manifest of your Server app, declare the Server subclass as a Service like so:

        <service android:name="net.prometheus3k.demo.interappcomms.server.MyAppCommsServer"
            android:exported="true" android:label="@string/app_name">
            <intent-filter>
                <action android:name="net.prometheus3k.demo.interappcomms.server.MyAppCommsServer" />
            </intent-filter>
        </service>

3. In your Client app, create an instance of SecureAppCommsClient:

        :::java
        mClient = new AppCommsClient(sContext, sServerPackage, sServerName);
**sServerPackage** and **sServerName** is the package that contains your subclass and the name of your subclass respectively, created in Step 1 e.g. "net.prometheus3k.demo.interappcomms.server" and "MyAppCommsServer"

4. Your Client app should implement Handler.Callback to handle any response to your message

5. To send a message, call AppCommsClient.sendMessage() passing your Handler.Callback.

**Secure AppComms:**

1. Create a subclass SecureAppCommsServer. You only need a constructor that passes through the SALT String to the super constructor.

2. In the manifest of your Server app, declare the Server subclass as a Service (See Step 2 of Simple App Comms above).

3. Create an instance of SecureAppCommsClient (see Step 3 of Simple App Comms above).

4. Your Client app should implement SecureAppCommsClient.AuthResultHandler to handle the result of the Auth Challenge.

5. To send an Auth Challenge, call SecureAppCommsClient.sendAuthChallenge() passing SALT string and AuthResultHandler in step 4.

### Contribution guidelines ###

* Feel free to make changes/bugfixes, but if you do so, please submit via merge/pull request.

### Who do I talk to? ###

* Me (Babu Madhikarmi) prometheus3k@gmail.com